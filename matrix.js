/**
 *
 */
class Matrix {
  /**
   * Initializes the new instance of Matrix class
   * @param value
   * The accepted types of "value" parameter are:
   *
   * 1. the object that exposes "rows" and "cols" and optional "create" properties
   * that specify number of rows and columns and the function that accepts the row and column number
   * and returns the value for the corresponding matrix item
   * example : new Matrix( {rows = 3, cols = 3, create = (r,c)=>1} ) creates the new 3 x 3 matrix
   * in which all the elements are set to 1.
   * if create property is not specified then all the matrix elements are set to zero
   *
   * 2. the sequence or array of real numbers that form the single matrix row
   * example 1 : new Matrix( 0.01, 0.03, 0.1, 0.3, 1, 3 ) creates the new 1x5 matrix
   * which looks like [0.01, 0.03, 0.3, 1 3]
   * example 2: new Matrix( [0.01, 0.03, 0.1, 0.3, 1, 3] ) creates the same matrix as in example 1
   *
   * 3. The sequence or array of real numbers arrays which form the matrix rows
   * example 1: new Matrix( [0.01, 0.03], [0.1, 0.3], [1,3] ) creates the new 3x2 matrix
   * which looks like
   * [0.01 0.03
   *  0.1  0.3
   *  1    3]
   *  example 2: new Matrix( [[0.01, 0.03], [0.1, 0.3], [1,3]] ) creates the same matrix as in example1
   */
  constructor(...value) {
    if(value.length === 1 && !Array.isArray(value[0])) {
      // maybe it's object with rows, cols properties is passed?
      let paramValue = value[0];
      if({}.hasOwnProperty.call(paramValue, 'rows') && {}.hasOwnProperty.call(paramValue, 'cols')) {
        if(!Number.isFinite(paramValue.rows)) {
          throw new Error('number of rows of a matrix should be the finite float numbers');
        }
        if(!Number.isFinite(paramValue.cols)) {
          throw new Error('number of columns of a matrix should be the finite float numbers');
        }
        console.log('creating the ' + paramValue.rows + 'x'+paramValue.cols + 'matrix');
        if({}.hasOwnProperty.call(paramValue, 'create') && typeof(paramValue.create)==='function') {
          for(let r = 0;r < paramValue.rows; r ++) {
            for(let c = 0; c < paramValue.cols;c ++) {
              let nextItem = paramValue.create(r,c);
              console.log('[', + r + ',' + c + '] = ' + nextItem );
            }
          }
        }
        return;
      }
    }
    if(value.length > 0) {
      let firstParamValue = value[0];
      let isSingleRow = !Array.isArray(firstParamValue);
      if(isSingleRow) {
        value.forEach(item => {
          "use strict";
          if(!Number.isFinite(item)) {
            throw new Error('all the matrix elements should be the finite float numbers');
          }
        });
        console.log('creating a matrix with the single row');
      }
      else{
        let colsNumber = firstParamValue.length;
        value.forEach(item => {
          "use strict";
          if(!Array.isArray(item) || item.length !== colsNumber){
            throw new Error('all the matrix rows should have the same number of columns');
          }
          item.forEach(subItem => {
            if(!Number.isFinite(subItem)) {
              throw new Error('all the matrix elements should be the finite float numbers');
            }
          })
        });
        console.log('creating a matrix with ' + value.length + ' rows and ' + colsNumber + ' columns');
      }
    }
  }
}

if (typeof Number.isFinite !== 'function') {
  Number.isFinite = function isFinite(value) {
    // 1. If Type(number) is not Number, return false.
    if (typeof value !== 'number') {
      return false;
    }
    // 2. If number is NaN, +∞, or −∞, return false.
    if (value !== value || value === Infinity || value === -Infinity) {
      return false;
    }
    // 3. Otherwise, return true.
    return true;
  };
}
//let c = (r,c)=>r+c;
new Matrix({rows : 5, cols: 4, create : (r,c)=>r+c});